<?php
namespace lommix\Bundle\LuxBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use lommix\Bundle\LuxBundle\Entity\User;


/**
 * Class Register
 * @package lommix\Bundle\LuxBundle\Service
 */
class Register
{
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $data
     * @return bool
     */
    public function registerUser($data)
    {
        $user = $this->em->getRepository('LuxBundle:User')->findOneByEmail($data['Email']);
        if (!$user) {
            $user = new User();
            $user->setEmail($data['Email']);
            $user->setUsername($data['Username']);
            $password = password_hash($data['Password'], PASSWORD_BCRYPT);
            $user->setPassword($password);
            $user->setRole('ROLE_USER');
            $this->em->persist($user);
            $this->em->flush();
            return true;
        }
        return false;
    }

}