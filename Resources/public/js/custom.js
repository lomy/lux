$(document).ready(function () {
    $('a.tree-toggler').click(function () {

        $(this).parent().children('ul.tree').height(400);
        if ($(this).parent().children('ul.tree').is(':hidden')) {
            $('ul.tree').hide();
            $(this).parent().children('ul.tree').slideDown(300);
        }
    });

    $('a.detail-toggle').click(function () {
        $('.detail-drop').hide();
        $('.detail-toggle').removeClass('active');
        $('.' + this.id).show();
        $(this).addClass('active');
    });
    $('a').each(function () {
        if ($(this).prop('href') == window.location.href) {
            if ($(this).parent().parent().hasClass('tree')) {
                $(this).parent().parent().height(400);
                $(this).parent().parent().show();
            }else{

                $(this).addClass('active');
            }
        }
    });

});