<?php
namespace lommix\Bundle\LuxBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Email', 'email')
            ->add('Password','password')
            ->add('Login', 'submit');
    }
    public function getName()
    {
        return 'Login';
    }
}