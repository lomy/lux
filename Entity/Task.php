<?php
namespace lommix\Bundle\LuxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lommix\Bundle\LuxBundle\Entity\Model\EntityModel;

/**
 * @ORM\Entity
 * @ORM\Table(name="projecttask")
 */
class Task extends EntityModel
{

    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $description;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $contributor;

    /**
     * @return mixed
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * @param mixed $contributor
     */
    public function setContributor($contributor)
    {
        $this->contributor = $contributor;
    }


    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="updates")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * Set project
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Project $project
     * @return Update
     */
    public function setProject(\lommix\Bundle\LuxBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \lommix\Bundle\LuxBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
}
