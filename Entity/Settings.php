<?php
namespace lommix\Bundle\LuxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lommix\Bundle\LuxBundle\Entity\Model\EntityModel;

/**
 * @ORM\Entity
 * @ORM\Table(name="settings")
 */
class Settings extends EntityModel
{


    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $notificationontask;
    /**
     * @ORM\Column(type="boolean")
     */
    private $notificationonbill;
    /**
     * @ORM\Column(type="boolean")
     */
    private $notificationonupdate;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $notificationemail;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $displaymode;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $displayname;

    public function __construct(){

    }
}
