<?php
namespace lommix\Bundle\LuxBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use lommix\Bundle\LuxBundle\Entity\Model\EntityModel;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="project")
 */

class Project extends EntityModel
{

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="projects")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @ORM\Column(type="integer", length=60)
     */
    protected $progress;
    /**
     * @ORM\Column(type="datetime", length=60)
     */
    protected $startedOn;

    /**
     * @ORM\Column(type="boolean", length=60)
     */
    protected $isFinished;
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $repository;
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $demo;
    /**
     * @ORM\Column(type="string", length=300)
     */
    protected $description;
    /**
     * @ORM\OneToMany(targetEntity="Update", mappedBy="project")
     */
    protected $updates;

    /**
     * @return mixed
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param mixed $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function getDemo()
    {
        return $this->demo;
    }

    /**
     * @param mixed $demo
     */
    public function setDemo($demo)
    {
        $this->demo = $demo;
    }

    /**
     * @ORM\OneToMany(targetEntity="Bill", mappedBy="project")
     */
    protected $bills;

    /**
     * @ORM\OneToMany(targetEntity="Task", mappedBy="project")
     */
    protected $tasks;

    public function __construct()
    {
        $this->updates = new ArrayCollection();
        $this->bills   = new ArrayCollection();
        $this->tasks   = new ArrayCollection();
        $this->isFinished = false;
        $this->progress   = 0;
    }

    /**
     * Set progress
     *
     * @param integer $progress
     * @return Project
     */
    public function setProgress($progress)
    {
        $this->progress = $progress;

        return $this;
    }

    /**
     * Get progress
     *
     * @return integer 
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Set startedOn
     *
     * @param \DateTime $startedOn
     * @return Project
     */
    public function setStartedOn($startedOn)
    {
        $this->startedOn = $startedOn;

        return $this;
    }

    /**
     * Get startedOn
     *
     * @return \DateTime 
     */
    public function getStartedOn()
    {
        return $this->startedOn;
    }

    /**
     * Set isFinished
     *
     * @param boolean $isFinished
     * @return Project
     */
    public function setIsFinished($isFinished)
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    /**
     * Get isFinished
     *
     * @return boolean 
     */
    public function getIsFinished()
    {
        return $this->isFinished;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set user
     *
     * @param \lommix\Bundle\LuxBundle\Entity\User $user
     * @return Project
     */
    public function setUser(\lommix\Bundle\LuxBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \lommix\Bundle\LuxBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add updates
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Update $updates
     * @return Project
     */
    public function addUpdate(\lommix\Bundle\LuxBundle\Entity\Update $updates)
    {
        $this->updates[] = $updates;

        return $this;
    }

    /**
     * Remove updates
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Update $updates
     */
    public function removeUpdate(\lommix\Bundle\LuxBundle\Entity\Update $updates)
    {
        $this->updates->removeElement($updates);
    }

    /**
     * Get updates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUpdates()
    {
        return $this->updates;
    }

    /**
     * Add bills
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Bill $bills
     * @return Project
     */
    public function addBill(\lommix\Bundle\LuxBundle\Entity\Bill $bills)
    {
        $this->bills[] = $bills;

        return $this;
    }

    /**
     * Remove bills
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Bill $bills
     */
    public function removeBill(\lommix\Bundle\LuxBundle\Entity\Bill $bills)
    {
        $this->bills->removeElement($bills);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * Add tasks
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Task $tasks
     * @return Project
     */
    public function addTask(\lommix\Bundle\LuxBundle\Entity\Task $tasks)
    {
        $this->tasks[] = $tasks;

        return $this;
    }

    /**
     * Remove tasks
     *
     * @param \lommix\Bundle\LuxBundle\Entity\Task $tasks
     */
    public function removeTask(\lommix\Bundle\LuxBundle\Entity\Task $tasks)
    {
        $this->tasks->removeElement($tasks);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTasks()
    {
        return $this->tasks;
    }
}
