<?php

namespace lommix\Bundle\LuxBundle\Entity\Model;
use Doctrine\ORM\Mapping as ORM;

abstract class EntityModel
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}