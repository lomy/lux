-- # Symfony2 based project managment system # --
-- # Lorenz Mielke www.lommix.de 2015        # --

-- Description:
The main goal of this application is to give customers a detailed overview
over their ordered product and easy access to all related files.

______________________________________________________

15.09.2015 - Proof of Concept work is done, next step will be the actual structure design of this application
             and decouple the segments of the current code for easy extension possibilities.
