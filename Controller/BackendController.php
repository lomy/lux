<?php

namespace lommix\Bundle\LuxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * This controller will do the admin backend for adding, updating and removing projects
 * Class BackendController
 * @package lommix\Bundle\LuxBundle\Controller
 */
class BackendController extends Controller
{
    /**
     *
     * @param Request $request
     */
    public function indexAction(Request $request)
    {

        return;
    }
}
