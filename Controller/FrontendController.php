<?php

namespace lommix\Bundle\LuxBundle\Controller;

use lommix\Bundle\LuxBundle\Entity\User;
use Proxies\__CG__\lommix\Bundle\LuxBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use lommix\Bundle\LuxBundle\Form\Type\LoginType;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * This controller will handle any request from users in the frontend
 * Class FrontendController
 * @package lommix\Bundle\LuxBundle\Controller
 */
class FrontendController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('LuxBundle:Main:Logintest.html.twig', array(
            'error' => $error
        ));
    }

    /**
     * @param $projectID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mainAction($projectID)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $user = $this->getUser();
        $project = $user->findProject($projectID);
        if (!$project && $projectID) {
            throw $this->createNotFoundException('This Project does not Exist');
        }

        return $this->render('LuxBundle:Frontend:index.html.twig', array(
            'user'     => $user,
            'project' => $project
        ));

    }
}
